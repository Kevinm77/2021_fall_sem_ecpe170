# A Stub to develop assembly code using QtSPIM

    # Declare main as a global function
    .globl main 

    # All program code is placed after the
    # .text assembler directive
    .text

# The label 'main' represents the starting point
main:
	#Register map
	la $s0, array		#s0 = array pointer
	lw $s1, arraySize	#s1 = arraySize
	lw $s2, search		#s2 = search
	lw $s3, result		#s3 = result	
	move $a0, $s0		#loading args
	addi $s1, $s1, -1	#arraySize - 1
	move $a1, $s1		#a1 = arraySize
	move $a2, $s2		#a2 = search
	jal arraySearch	#call function
	sw $v0, result		#store the return value into results
	lw $t0, result		#load result into t0 for checks
	
	bge $t0, 0, else0	#If result < 0
	li $v0, 4
	la $a0, notFound	#print search key not found
	syscall
	j endIf0		#skip else statement
else0:
	li $v0, 4		
	la $a0, found0		#print "Element " 
	syscall
	li $v0, 1
	move $a0, $s2		#print search variable
	li $v0, 4
	la $a0, found1		#print "found at array index
	syscall
	li $v0, 1
	move $a0, $t0		#print t0
	syscall
	li $v0, 4
	la $a0, newLine	#print newLine
	syscall
	
endIf0:


	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit
	
#---------------------
#Recursive function || ARRAY SEARCH
arraySearch:
	#result; set as $t2 and 0 for now
	bne $t0, 0, search1 #if this is the first jump then save j add
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	#store original ra address
search1:
	li $t7, 4		#t7 = 4 since I can't mul constants
	mul $t1, $t7, $a1 	#t0 = 4 * shift/index
	add $t2, $t1, $a0	#t1 = t0 + base add of array
	lw $t4, 0($t2)		#t4 = value at add t2
   	bne $t4, $a2, elseIf	#if( array[arraySize] == search
   	move $t5, $a1		#t5 = arraySize, which is my temp Result
	j endIf
elseIf:
	bne $a1, -1, else	#if arraySize == 1
	li $t5, -1		#t5 = -1
	j endIf
else:
	#storing args for next resursive call
	#I would move the address into a0 but it already has it and nothing changes a0
	addi $a1, $a1, -1	#a1 = arraySize - 1	
	#I also would store a2, but it already has it
	jal arraySearch
	move $t5, $v0

endIf:
	#lw $v0, result
	move $v0, $t5		
	lw $ra, 0($sp)		
	addi $sp, $sp, 4
	jr $ra



    # All memory structures are placed after the
    # .data assembler directive
    .data

array: .word 2, 3, 5, 7, 11
arraySize: .word 5
search: .word 0
result: .word 0
notFound: .asciiz "Search Key not found\n" 
found0: .asciiz "Element "
found1: .asciiz "found at array index "
newLine: .asciiz "\n"
