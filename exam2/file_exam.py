#!/usr/bin/env python3

import argparse
import ctypes
import random
import socket
import struct
import sys

def main():


 
	#Create UDP socket

	server_ip='10.10.4.50'
	port=3456
	dest_addr=(server_ip, port)

	s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	protocol=1
	number1 = 5
	number2 = 5
	nameLength = 5
	myName = 'Kevin'


	raw_bytes = bytearray()
	raw_bytes += struct.pack("!B", protocol)
	raw_bytes += struct.pack("!L", number1)
	raw_bytes += struct.pack("!L", number2)
	raw_bytes += struct.pack("!L", nameLength)
	raw_bytes += bytes(myName, 'ascii')

	#print(raw_bytes)

	bytes_sent=s.sendto(raw_bytes, dest_addr)
	(bytes1, src_addr) = s.recvfrom(4096)


	# Close socket
	# ---------
	# STUDENT TO-DO
	# ---------
	s.close()
	
	unpacked = struct.unpack("!BHL", bytes1)
	#unpacked.split(",")
	if(unpacked[1] == 1):
		print("Success")
	else:
		print("Failure")
		
	print(str(number1) + " + " + str(number2) + " = " + str(unpacked[2]))

if __name__ == "__main__":
	sys.exit(main())
