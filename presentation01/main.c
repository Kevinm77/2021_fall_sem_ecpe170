#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
int main(){
	int size;
	struct node* head;
	
	printf("\n\n Linked List : To create and display Singly Linked List :\n");
	printf("-------------------------------------------------------------\n");
	
	printf(" Input the number of nodes : ");
	scanf("%d", &size);

	head = createNodeList(size);

	if(size > 0){
		printf("\n Data entered in the list : \n");	
	}

	displayList(head, size);

	free_Linked_List(head, size); 
	//displayList(head, size);

	return 0;
}
