#ifndef FUNCTIONS_H
#define FUNCTIONS_H

struct node{
	int num;
	struct node *next;
};

struct node* createNodeList(int n);
void displayList(struct node* head, int size);
void free_Linked_List(struct node* head, int size);
#endif
