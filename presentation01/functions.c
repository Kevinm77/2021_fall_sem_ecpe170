#include "functions.h"
#include <stdio.h>
#include <stdlib.h>

struct node* createNodeList(int size)
{
    struct node *newNode, *temp, *head;
    int num, i;
    
    if(size == 0){
		printf("Linked List would have no elements. \n");
    }
    else
    {
// reads data for the node through keyboard
		head = (struct node *)malloc(sizeof(struct node));
		if(head == NULL) {
        		printf(" Memory can not be allocated.");
    		}
    		else{
			printf(" Input data for node 1 : ");
			scanf("%d", &num);
			head->num = num;      
			head->next = NULL; // links the address field to NULL
			temp = head;
		// Creating n nodes and adding to linked list
		
			for(i=2; i<=size; i++){
		    		newNode = (struct node *)malloc(sizeof(struct node));
		    		if(newNode == NULL){
		     	   		printf(" Memory can not be allocated.");
		     	   		break;
		    		}
		   		else{
				    printf(" Input data for node %d : ", i);
				    scanf(" %d", &num);
		 
				    newNode->num = num;      // links the num field of newNode with num
				    newNode->next = NULL; // links the address field of newNode with NULL
		 
				    temp->next = newNode; // links previous node i.e. temp to the newNode
				    temp = temp->next; 
		    		}
			}
			temp->next = head;
		}
        
    }
	return head;
}
void displayList(struct node* head, int size)
{
    struct node *temp;
    if(size == 0)
    {
        printf(" Cannot Display. List is empty. \n");
    }
    else
    {
    	int i = 0;
    	temp = head;
    	while(i < size){
    		printf(" Data = %d\n", temp->num);
    		temp = temp->next;
    		i++;
	}
    	/*
        temp = head;
        do{
            printf(" Data = %d\n", temp->num);       // prints the data of current node
            temp = temp->next;                       // advances the position of current node
        }while(temp != head);
        */
    }
} 

void free_Linked_List(struct node* head, int size){
	struct node* temp;
	if(size == 0){
		printf(" Cannot free. Empty list\n");
	}
	else{
		int i = 0;
		while(i < size){
			temp = head;
			head = head->next;
			free(temp);
			i++;
		}
	}

}
