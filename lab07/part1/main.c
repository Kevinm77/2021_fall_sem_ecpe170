#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main(){
	uint32_t array1[2][3]; // 2D array
	uint32_t array2[2][2][3];  // 3D array
	int i, j, k;
	printf("2D Array: \n");
	for(i = 0; i < 2; i++){
		for(j = 0; j < 3; j++){
			printf("array1[%i][%i] - Memory Address %p\n", i, j, &array1[i][j]);
		}
		printf("\n");
	}
	
	//The following for loop is just to show address of array1[i][j] but with different nested loops
	for(j = 0; j < 3; j++){
		for(i = 0; i < 2; i++){
			printf("array1[%i][%i] - Memory Address %p\n", i, j, &array1[i][j]);
		}
		printf("\n");
	}
	
	printf("\n3D Array: \n");
	
	for(i = 0; i < 2; i++){
		for(j = 0; j < 2; j++){
			for(k = 0; k < 3; k++){
				printf("array2[%i][%i][%i] - Memory Address %p\n", i, j, k, &array2[i][j][k]);
			} 
			printf("\n");
		}
	}
}
