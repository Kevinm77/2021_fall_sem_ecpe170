#!/bin/bash
declare -a arr=() #Declare empty array to store output of text

check=1

echo "Saving the results into output.txt"
for((i = 256; i <= 2048; i += 256)) # Iterate through various sizes
do
	
	#Use a for loop with check that iterates through the two types of algorithms
	for((check = 1; check <= 2; check += 1))do
		j=0 #Reset index for the new iteration of ./matrix_math
	
		./matrix_math $check $i > algorithm.txt #run matrix program and store output in algorithm.txt
		while read line
		do
			arr[$j]="$line" #copy the output of matrix into an array
			j=$((j+1)) #Iterate j so that matrix is sequentially filled
		
		done < algorithm.txt #Read the entirety of algorithm using while loop

		echo "Algorithm $check | Size: ${arr[0]:39}  ${arr[3]:24} FPO/s    " &>> output.txt
		
	done
		echo -e "" &>> output.txt
		check=0
		
done




