# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
#Finds the gcd for 10 random pairs of ints using 3 different functions
main:   
	lw $s0, n1	#s0 = n1
	lw $s1, n2	#s1 = n2
	lw $s2, i	#s2 = i
	li $t0, 10	#t0 = 10
	
	
for:	bge $s2, $t0, endFor		#for i < 10
	li $a0, 1			#load arguments a0 = 1
	li $a1, 10000			#a1 = 10000
	jal randomInRange		#jump to function RandominRange
	move $s0, $v0			#Store return value in s0
	jal randomInRange		#jump to function randominRange
	move $s1, $v0			#store return value in s1
	
	li $v0, 4			#load v0 with 4 to print
	la $a0, msg			#store address of first message
	syscall

	li $v0, 1			#load v0 with 1 to print 
	move $a0, $s0			#move register s0 into a0 to printint
	syscall
	
	li $v0, 4			#load v0 with 4 toprint			
	la $a0, msg1			#store address of second message
	syscall
	
	li $v0, 1			#load v0 with 1 to print
	move $a0, $s1			#move register s1 into a0 to printint
	syscall
	
	li $v0, 4			#load v0 with 4 to print
	la $a0, msg2			#store address of third message
	syscall
	
	li $t6, 0			#t6 will be used to store future ra address in gcd
	move $a0, $s0			#load arguments, s0 into a0
	move $a1, $s1			#s1 into a1
	jal gcd			#jump to function with preloaded arguments
	move $s5, $v0			#store return value which is the gcd
	
	li $v0, 1			#load v0 with 2 to print
	move $a0, $s5			#move register a0 with s5 to print int
	syscall
	
	li $v0, 4			#load v0 with 4 to print
	la $a0, msg3			#store address of fourth message
	syscall
	
	addi $s2, $s2, 1		#i++
	j for				#jump back to for loop
endFor:

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

#random in range function below
#grabs a random number in a range
randomInRange:
	addi $sp, $sp, -4	#Store values in registers which will be changed
	sw $ra, 0($sp)		#In order to prevent overrides
	addi $sp, $sp, -4	#Store return address, s0, s1, and t0
	sw $s0, 0($sp)
	addi $sp, $sp, -4
	sw $s1, 0($sp)
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	
	sub $t0, $a1, $a0	#high - low = $a1 - $a0
	addi $s0, $t0, 1	#s0 = range = t0 + 1
	
	jal getRandom		#jump to function get random
	move $s1, $v0		#s1 will store the random
	
	divu $v0, $s0		#randnum % range
	mfhi $t2		#which is in t2 now
	add $t2, $t2, $a0	#t2 + low
	move $v0, $t2		#return t2
	
	lw $t0,0($sp)		#Unpopping all values that were stores
	addi $sp, $sp, 4	#	- t0, s0, s1, return address
	lw $s1, 0($sp)
	addi $sp, $sp, 4
	lw $s0, 0($sp)
	addi $sp, $sp, 4
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	
	jr $ra			#jump to return address
	
#get random function below
#creates a random number
getRandom:
	addi $sp, $sp, -4	#Store all future changed register
	sw $t0, 0($sp)		#Adust stack pointer
	addi $sp, $sp, -4	#Stores values from
	sw $s0, 0($sp)		#	- t0, s0, s1
	addi $sp, $sp, -4
	sw $s1, 0($sp)
	
	#result will be in v0
	lw $s0, m_z		#m_z will be s0
	lw $s1, m_w		#m_w will be s1
	
	li $t0, 36969		#t0 = 36969
	and $t1, $s0, 65535	#t1 = m_z & 65535
	srl $t2, $s0, 16	#t2 = m_z >> 16
	mul $t3, $t0, $t1	#t3 = 36969 * (m_z & 65535)
	addu $t3, $t3, $t2	#t3 += (m_z >> 16)
	sw $t3, m_z		#m_z = t3
	
	li $t0, 18000		#t0 = 18000
	and $t1, $s1, 65535	#t1 = m_w & 65535
	srl $t2, $s1, 16	#t2 = m_w >> 16
	mul $t3, $t0, $t1	#t3 = 18000 * (m_z & 65535)
	addu $t3, $t3, $t2	#t3 += (m_z >> 16)
	sw $t3, m_w		#m_w = t3
	
	lw $s0, m_z		#update $s0
	lw $s1, m_w		#update $s1
	
	sll $t3, $s0, 16	#t3 = m_z << 16
	addu $t3, $t3, $s1	#t3 += s1, t3 += m_w & 65536
	move $v0, $t3		#v0 = t3, v0 returns the value in the function
	
	lw $s1, 0($sp)		#Unpop all stored values in this function
	addi $sp, $sp, 4	#	- s1, s0, t0
	lw $s0, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	
	
	jr $ra			#jump to return address in main

gcd:	
	bne $t6, $zero, if	#Check if this is the first iteration of gcd
	addi $sp, $sp, -4	#If it is store the return address
	sw $ra, 0($sp)
	addi $t6, $t6, 1	#after first iteration, the new return addresses are not saved


if:     beq $a1, 0, else	#check if n2 == 0
	divu $a0, $a1		#if it doesnt then keep searching for gcd
	mfhi $t7		#keep remainder
	move $a0, $a1		#store n1 as n2
	move $a1, $t7		#store n2 as the remainder
	jal gcd		#recursively find gcd
	
else:	move $v0, $a0		#if n2 == 0 then store the gcd in v0
	lw $ra, 0($sp)		#load the original return address
	addi $sp, $sp, 4	#adjust stack pointer
	jr $ra			#jump back to main return address
		
	# All memory structures are placed after the
	# .data assembler directive
	

	.data
	n1: .word 0
	n2: .word 0
	i: .word 0
	m_w: .word 50000
	m_z: .word 60000
	msg: .asciiz "\n G.C.D of "
	msg1: .asciiz " and "
	msg2: .asciiz " is "
	msg3: .asciiz "."


	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
