# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	#Register Map
	lw $s1, result		#s1 = result
	
	li $v0, 8		#v0 = 8
	la $a0, str		#a0 = &str[0]
	li $a1, 256		#a1 = 256 length of string buffer	
	move $t0, $a0		#t0 = a0
	sw $t0, str		#str = t0	
	syscall

	la $t1, str		#t1 = &str[0]

	lb $t2, 0($t1)		#t2 = str[0]
while: beq $t2, $zero, endWhile#while t2 != null

if:	bne $t2, 'e', endif	#if t2 != e then jump endif
	add $s1, $zero, $t1	#else s1 = t1 = &str[i]
	j endWhile		#break from while
endif:
	addi $t1, $t1, 1	#increment t1
	lb $t2, 0($t1)		#t2 = str[i]
	j while		#jump to while

endWhile:

if1:	beq $s1, $zero, else	#if s1 == null jump to else
	li $v0, 4		#v0 = 4
	la $a0, out		#a0 = &out
	syscall		#print the output of out
	
	li $v0, 1		#v0 = 1
	move $a0, $t1		#a0 = t1
	syscall		#print the byte that was found
	
	li $v0, 4		#v0 = 4
	la $a0, lf		#a0 = &lf
	syscall		#print new line
	
	li $v0, 4		#v0 = 4
	la $a0, out1		#a0 = &out1
	syscall		#print the output of out1

	
	li $v0, 11		#v0 = 11
	lb $a0, 0($s1)		#a0 = value of &s1
	syscall		#print the byte at &s1
		
	li $v0, 4		#v0 = 4
	la $a0, lf		#a0 = &lf
	syscall		#print new line
	
	
	
	j endif1
else:				#if no character matched print
	li $v0, 4		#v0 = 4
	la $a0, out2		#a0 = &out2
	syscall		#print the output of out2
	
endif1:
	sw $s1, addr		#store the address of results or s1 from s1
	
	sw $t2, result		#store the character in results from t2
	
	#li $v0, 4
	#la $a0, str	loads the address 
	#syscall

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	i: .word 0
	addr: .space 20
	str: .space 256				#Scan input for a string
	result: .word 0				#Set results to null
	out: .asciiz "First Match at address: "
	out1: .asciiz "The matching character is: "
	out2: .asciiz "No Match Found"
	lf: .asciiz "\n"
	
	
	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
