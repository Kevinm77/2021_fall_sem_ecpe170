# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:   #Register map s0 = A, s1 = B, ... s6 = Z
        #t0 = A + B, t1 = C - D, t2 = E + F, t3 = A - C, t4 = t0 + t1, t5 = t2 - t3
	lw $s0, A 	  #s0 = A
	lw $s1, B 	  #s1 = B
	lw $s2, C 	  #s2 = C
	lw $s3, D 	  #s3 = D
	lw $s4, E 	  #s4 = E
	lw $s5, F	  #s5 = F
	lw $s6, Z	  #s6 = Z
	
	add $t0, $s0, $s1 # t0 = A + B
	sub $t1, $s2, $s3 # t1 = C - D
	add $t2, $s4, $s5 # t2 = E + F
	sub $t3, $s0, $s2 # t3 = A - C
	
	add $t4, $t0, $t1 # t4 = t0 + t1
	sub $t5, $t2, $t3 # t5 = t2 - t3
	
	add $s6, $t4, $t5 # s6 = t4 + t5
	sw $s6, Z	   # Z = s6 
	
	
	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data
	
	A: .word 15
	B: .word 10
	C: .word 7
	D: .word 2
	E: .word 18
	F: .word -3
	Z: .word 0

	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
