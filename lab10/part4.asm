# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	#Register map
	#s0 = &A[0]
	#s1 = &B[0]
	#s2 = C
	#s3 = i
	la $s0, A		#s0 = &A[0]
	la $s1, B		#s1 = &B[0]
	lw $s2, C		#s2 = C
	lw $s3, i		#s3 = i
	addi $s3, $zero, 0	#s3 = 0
	addi $t0, $zero, 5	#t0 = 5
	
for:	bge $s3, $t0, endfor	#if s3 >= t0 == i >= 5 then end for
	add $t1, $s3, $s3	#t1 = 2 * i
	add $t1, $t1, $t1	#t1 = 4 * i
	add $t2, $s1, $t1	#t2 = address of b[i] 
	add $t6, $s0, $t1	#t6 = address of a[i]
	lw $t3, 0($t2)		#t3 = value at address t2
	add $t4, $t3, $s2	#t4 = t3 + s2 = B[i] + C
	sw $t4, 0($t6)		#Store t4 in address of s0 = A[i] = t4
	
	addi $s3, $s3, 1	#increment s3 or i
	j for

endfor:
	addi $s3, $s3, -1	#decrement s3 or i
	addi $t5, $zero, 1
while:	blt $s3, $zero, endWhile #while i >= 0
	
	lw $t3, 0($t6)		#load t3 = A[i]
	add $t3, $t3, $t3	#t3 = A[i] * 2
	sw $t3, 0($t6)		#A[i]=t3
	add $t1, $t5, $t5	#t1 = 2 * i
	add $t1, $t1, $t1	#t2 = 4 * i
	sub $t6, $t6, $t1	#t6 = address of A[i]
	
	addi $s3, $s3, -1  	#i--
	j while
	
endWhile:
	

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	i: .word 0
	A: .space 20
	B: .word 1, 2, 3, 4, 5
	C: .word 12
	
	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
