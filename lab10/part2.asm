# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	lw $s0, A 		#s0 = A
	lw $s1, B 		#s1 = B
	lw $s2, C 		#s2 = C
	lw $s3, Z		#s3 = Z
	
	addi $t0, $zero, 1	#temp0 = 1
	addi $t1, $zero, 2	#temp1 = 2
	addi $t2, $zero, 3	#temp2 = 3
	addi $t3, $zero, 5	#temp3 = 5
				# if A > B or C < 5
	ble $s0, $s1, if	#if A <= B then jump to if
	add $s3, $zero, $t0 	# if not then s3 = 1	
	j switch		#jump to switch label

if:	bge $s2, $t3, elseif	#if C >= 5 then jump to else if
	add $s3, $zero, $t0	#if not then s3 = 1
	j switch		#jump to switch label
	
elseif: add $t4, $s2, $t0	#temp4 = C + 1
	addi $t5, $zero, 7	#temp5 = 7
	ble $s0, $s1, else	#else if A > B and C + 1 == 7
	bne $t4, $t5, else	# ^
	add $s3, $zero, $t1	#if those are false then s3 = 2
	j switch		#jump to switch label

else: 	add $s3, $zero, $t2 	#else s3 = 3
	j switch		#jump to switch label
	
switch: beq $s3, $t0, case1	#Check s3 with 1, if they equal jump to case1
	beq $s3, $t1, case2	#Check s2 with 2, if they equal jump to case2
	j default		#If the statements before were not true jump to default case

case1:	sub $s3, $s3, $t1	#s3 = 0 - 1 = -1
	j break1		#Exit switch statement

case2:	sub $s3, $zero, $t1	#s3 = 0 - 2 = -2
	j break1		#Exit switch statement

default:add $s3, $zero, $zero	#s3 = 0 + 0 = 0
	j break1		#Exit switch statement

break1: sw $s3, Z		#Store s3 into Z

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	A: .word 10
	B: .word 15
	C: .word 6
	Z: .word 0
	
	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
