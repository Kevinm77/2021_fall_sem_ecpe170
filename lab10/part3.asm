# A Stub to develop assembly code using QtSPIM

	# Declare main as a global function
	.globl main 

	# All program code is placed after the
	# .text assembler directive
	.text 		

# The label 'main' represents the starting point
main:
	lw $s0, Z 			#s0 = Z
	lw $s1, i			#s1 = i
	
	addi $t0, $zero, 20		#temp0 = 20
	addi $t1, $zero, 1		#temp1 = 1
	addi $t2, $zero, 2		#temp2 = 2
	addi $t3, $zero, 100		#temp3 = 100
#--------------	
while0:bne $t1, $t1, endWhile0 	# Exit while loop if 1 != 1
	bgt $s1, $t0, endWhile0 	#if i > 20 then exit while loop
	add $s0, $s0, $t1	 	#s0 = s0 + temp1 = s0 + 1
	add $s1, $s1, $t2	 	#s1 = s1 + temp2 = s1 + 2
	j while0		 	#Jump back to while loop


endWhile0: 				#Marks the end of the while so kind of like '}'
#--------------
do:	add $s0, $s0, $t1		#s0 = s0 + temp1 = s0 + 1
	bge $s0, $t3, endWhile1	#if s0 >= 100 then exit do-while loop
	j do

endWhile1:
#--------------
while2: ble $s1, $zero, endWhile2	#exit while loop if i <= 0
	sub $s0, $s0, $t1		#s0 = s0 - temp1 = s0 - 1
	sub $s1, $s1, $t1		#s1 = s1 - temp1 = s1 - 1
	j while2			#jump back to while loop
	
endWhile2:
	sw $s0, Z

#--------------
	
	

	# Exit the program by means of a syscall.
	# There are many syscalls - pick the desired one
	# by placing its code in $v0. The code for exit is "10"

	li $v0, 10 # Sets $v0 to "10" to select exit syscall
	syscall # Exit

	# All memory structures are placed after the
	# .data assembler directive
	.data

	i: .word 0
	Z: .word 2
	
	# The .word assembler directive reserves space
	# in memory for a single 4-byte word (or multiple 4-byte words)
	# and assigns that memory location an initial value
	# (or a comma separated list of initial values)
	#For example:
	#value:	.word 12
